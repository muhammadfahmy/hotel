from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Hotel(models.Model):
    name = models.CharField(max_length=256, blank=True, null=True)

    stars = models.FloatField(blank=True, null=True)
    location = models.CharField(max_length=256, blank=True, null=True)
    rating = models.FloatField(blank=True, null=True)
    total_ratings = models.IntegerField(blank=True, null=True)
    active = models.NullBooleanField(default=False, blank=True, null=True)
    owner = models.OneToOneField(
        User, on_delete=models.CASCADE, blank=True, null=True)


class Room(models.Model):
    price = models.FloatField()
    image = models.ImageField(upload_to="room_image",
                              default="room_image/hotel-luxury.jpg")
    room_type = models.CharField(max_length=256)
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)


class Reservation(models.Model):
    room = models.ForeignKey('Room', on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    total_price = models.FloatField(null=True, blank=True)
    broker_commission = models.FloatField(null=True, blank=True)
