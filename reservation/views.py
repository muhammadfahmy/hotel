from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic.edit import FormView
from .forms import *
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.list import ListView
from django.shortcuts import get_object_or_404, render_to_response, redirect

def Usersignup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/user/home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if (user.is_staff==1) and (user.is_superuser==1):
                login(request, user)
                return redirect('/broker/home')
            elif (user.is_staff==1) and (user.is_superuser == 0):
                login(request, user)
                return redirect('/owner/home/'+ str(user.hotel.id))
            elif user.is_active:
                login(request, user)
                return redirect('broker/home')
            else:

                HttpResponse("User is inacvtive")

        else:
            return HttpResponse("Invalid login details given")
    else:
        return render(request, 'login.html', {})


def brokersignup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            user.is_staff = True
            user.is_superuser = True
            user.save()
            login(request, user)
            return redirect('broker/home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def Ownersignup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            user.is_staff = True
            user.save()
            hotel = Hotel.objects.create(owner=user)
            hotel.save()
            login(request, user)
            return redirect('/owner/hotel/'+ str(user.hotel.id))
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})



class HotelUpdateView(UpdateView):
    model = Hotel
    form_class = HotelForm
    template_name = 'HotelOwner.html'


    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        print(self.object.id)
        return redirect('room/'+ str(self.object.id))




class RoomListView(ListView):
    model = Room
    template_name = 'RoomListview.html'
    context_object_name = 'room_list'

    

def HotelActivate(request, pk ,**kwargs):
    hotel = Hotel.objects.get(pk=pk)
    hotel.active = 1
    hotel.save()
    return redirect('/broker/home')

def Hoteldiactivate(request, pk ,**kwargs):

    hotel = Hotel.objects.get(pk=pk)
    hotel.active = 0
    hotel.save()
    return redirect('/broker/home')

class UserHotelsList(ListView):

    queryset = Hotel.objects.all().filter(active=1)
    template_name = 'hotel_list.html'
    


class brokerHotelsList(ListView):

    context_object_name = 'hotel_list'
    model = Hotel
    template_name = 'brokerHotelList.html'




class ReservationView(FormView):
    template_name = 'ReservationForm.html'
    form_class = ReservationForm
    success_url = '/'

    def dispatch(self, request, **kwargs):
        room_pk = self.kwargs['pk']
        print(room_pk)
        self.room = get_object_or_404(Room, pk=room_pk)
        print(self.room)
        return super(
            ).dispatch(request, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(room=self.room)
        return context

    def form_valid(self, form):
        form.save(room=self.room)
       
        delta = form.instance.end_date-form.instance.start_date
        form.instance.total_price = delta.days * form.instance.room.price
        form.instance.broker_commission = (0.09) * form.instance.total_price
        super().form_valid(form)
        return HttpResponse(str(delta.days) + ' days ' + '|| User price: ' + str(form.instance.total_price) +'  || Broker Commision : ' + str(form.instance.broker_commission))



class RoomCreateView(FormView):
    template_name = 'AddRoom.html'
    form_class = RoomForm
    success_url = '/'

    def dispatch(self, request, **kwargs):
        hotel_pk = self.kwargs['pk']
        print(hotel_pk)
        self.hotel = get_object_or_404(Hotel, pk=hotel_pk)
        print(self.hotel)
        return super(
            ).dispatch(request, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(hotel=self.hotel)
        return context

    def form_valid(self, form, **kwargs):
       
        
        super().form_valid(form)
        return HttpResponse('Room Created!')
