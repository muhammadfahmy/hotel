from .models import *
from django import forms
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import HttpResponse, HttpResponseRedirect


class HotelForm(forms.ModelForm):
    class Meta:
        model = Hotel
        fields = ['name', 'location']


class DateInput(forms.DateInput):
    input_type = 'date'


class ReservationForm(forms.ModelForm):
    def save(self, room):
        instance = super(ReservationForm, self).save(commit=False)
        instance.room = room
        instance.save()
        return instance

    class Meta:
        model = Reservation
        fields = ['start_date', 'end_date']
        widgets = {
            'start_date': DateInput(attrs={'type': 'date'}),
            'end_date': DateInput(attrs={'type': 'date'})

        }


class RoomForm(forms.ModelForm):
    def save(self, room):
        instance = super(RoomForm, self).save(commit=False)
        instance.hotel = hotel
        instance.save()
        return instance

    class Meta:
        model = Room
        fields = ['price', 'image', 'room_type']
