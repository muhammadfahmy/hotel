
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from reservation.views import *
from django.conf import settings
from django.conf.urls.static import static
import os

ROOT = os.getcwd()
MEDIA_ROOT = os.path.join(ROOT, 'uploaded_images')
urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', user_login,
         name='login'),

    path('logout/', auth_views.logout, name='logout'),
    path('user/signup/', Usersignup, name='Usersignup'),
    path('broker/signup/', brokersignup, name='brokersignup'),
    path('owner/signup/', Ownersignup, name='Ownersignup'),
    path('owner/hotel/<int:pk>', HotelUpdateView.as_view(), name='HotelForm'),
    path('owner/hotel/room/<int:pk>', RoomCreateView.as_view(), name='RoomForm'),
    path('broker/home', brokerHotelsList.as_view()),
    path('broker/hotel/<int:pk>/acivate', HotelActivate, name='HotelActivate'),
    path('broker/hotel/<int:pk>/diacivate',
         Hoteldiactivate, name='Hoteldiactiate'),
    path('user/home', UserHotelsList.as_view()),
    path('user/hotel/<int:pk>/rooms', RoomListView.as_view(), name="UserRoomList"),
    path('room/<int:pk>', ReservationView.as_view(), name='ReservationForm'),
]

urlpatterns += static('/media/', document_root=MEDIA_ROOT)
